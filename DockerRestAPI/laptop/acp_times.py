"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from math import modf

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

table = [(15,34),(15,32),(15,30),(11.428, 28)]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    km_left = control_dist_km
    open_time = arrow.get(brevet_start_time)
    if (control_dist_km < 0):
      open_time = open_time.shift(hours=-2)
    elif(control_dist_km > brevet_dist_km * 1.2):
       open_time = open_time.shift(hours=-1) # So we can catch it later and show an error
    else: 
      if(brevet_dist_km < control_dist_km <= brevet_dist_km * 1.2):
        km_left = brevet_dist_km    
      if(km_left > 600):
        mins, hour = modf((km_left - 600) / table[3][1])
        tot_mins = round(60 * mins)
        open_time = open_time.shift(hours=+hour, minutes=+tot_mins)
        km_left = km_left - (km_left - 600)
      if(km_left > 400):
        mins, hour = modf((km_left - 400) / table[2][1])
        tot_mins = round(60 * mins)
        open_time = open_time.shift(hours=+hour, minutes=+tot_mins)
        km_left = km_left - (km_left - 400)
      if(km_left > 200):
        mins, hour = modf((km_left - 200) / table[1][1])
        tot_mins = round(60 * mins)
        open_time = open_time.shift(hours=+hour, minutes=+tot_mins)
        km_left = km_left - (km_left - 200)
      if(km_left > 0):
        mins, hour = modf(km_left / table[0][1])
        tot_mins = round(60 * mins)
        open_time = open_time.shift(hours=+hour, minutes=+tot_mins)
        km_left = 0    

    return open_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    limits = {200: (13,30), 300: (20,00), 400: (27,00), 600:(40,00), 1000: (75,00)}

    km_left = control_dist_km
    close_time = arrow.get(brevet_start_time)

    if (control_dist_km < 0):
      close_time = close_time.shift(hours=-2)
    elif(control_dist_km < 60):
      mins, hour = modf(km_left / 20)
      tot_mins = round(60 * mins)
      close_time = close_time.shift(hours=+hour + 1, minutes=+tot_mins)
    elif(control_dist_km > brevet_dist_km * 1.2):
       close_time = close_time.shift(hours=-1) # So we can catch it later and show an error
    else: 
      if(brevet_dist_km <= control_dist_km <= brevet_dist_km * 1.2):
        close_time = close_time.shift(hours=+limits[brevet_dist_km][0], minutes=+limits[brevet_dist_km][1])
      else:     
        if(km_left > 600):
          mins, hour = modf((km_left - 600) / table[3][0])
          tot_mins = round(60 * mins)
          close_time = close_time.shift(hours=+hour, minutes=+tot_mins)
          km_left = km_left - (km_left - 600)
        if(km_left > 400):
          mins, hour = modf((km_left - 400) / table[2][0])
          tot_mins = round(60 * mins)
          close_time = close_time.shift(hours=+hour, minutes=+tot_mins)
          km_left = km_left - (km_left - 400)
        if(km_left > 200):
          mins, hour = modf((km_left - 200) / table[1][0])
          tot_mins = round(60 * mins)
          close_time = close_time.shift(hours=+hour, minutes=+tot_mins)
          km_left = km_left - (km_left - 200)
        if(km_left > 0):
          mins, hour = modf(km_left / table[0][0])
          tot_mins = round(60 * mins)
          close_time = close_time.shift(hours=+hour, minutes=+tot_mins)
          km_left = 0 


    return close_time.isoformat()