<html>
    <head>
        <title>CIS 322 REST-api</title>
    </head>

    <body>
        <h1>List all</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listAll');
            $obj = json_decode($json);
	          $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List open only</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listOpenOnly');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List close only</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listCloseOnly');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List all csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listAll/csv');
            $obj = json_decode($json);
            echo "<li>$obj</li>";
            ?>
        </ul>
        <h1>List open only csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listOpenOnly/csv');
            $obj = json_decode($json);
            echo "<li>$obj</li>";
            ?>
        </ul>
        <h1>List close only csv</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listCloseOnly/csv');
            $obj = json_decode($json);
            echo "<li>$obj</li>";
            ?>
        </ul>
         <h1>List all json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listAll/json');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List open only json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listOpenOnly/json');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List close only json</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listCloseOnly/json');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List open only csv top 3</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listOpenOnly/csv?top=3');
            $obj = json_decode($json);
            echo "<li>$obj</li>";
            ?>
        </ul>
        <h1>List open only json top 5</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listOpenOnly/json?top=5');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        <h1>List close only csv top 6</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listCloseOnly/csv?top=6');
            $obj = json_decode($json);
            echo "<li>$obj</li>";
            ?>
        </ul>
        <h1>List close only json top 4</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5001/listCloseOnly/json?top=4');
            $obj = json_decode($json);
              $times = $obj->Array;
            foreach ($times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
    </body>
</html>
