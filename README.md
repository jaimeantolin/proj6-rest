# Project 6: Brevet time calculator service

By: Jaime Antolin Merino

Simple listing service from project 5 stored in MongoDB database.

## What is in this repository

To understand all previous functionallity on what is implemented in terms of the brevet calculator please refer to project 5 README.


## Functionality added


* RESTful service to expose what is stored in MongoDB with the following functionalities given through APIs (JSON is default representation):
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Also added a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

* Designed a consumer program to use the services exposed. Consumer program is in a different container to the calculator and to the db.

## Mapping

* Brevet calculator can be accesed through "localhost:5001", here is where you will add things to the end of it to use the APIs

* Consumer program website can be accesed at "localhost:5000" - this shows how different calls to the API collect different data in different formats

